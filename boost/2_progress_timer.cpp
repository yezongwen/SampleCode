/* note my boost install path is "/c/project/opensource/008boost/boost_1_60_0"
 * compile with gcc 4.8.1
 * g++ 2_progress_timer.cpp -I/c/project/opensource/008boost/boost_1_60_0 -o progress_timer.exe
 * this is a demo to show the usage of progress_timer
 * 
*/

#include <iostream>
#include <unistd.h>
#include "boost/progress.hpp"
using namespace boost;
using namespace std;

int main(int argc, char * argv[])
{
	// auto out consumed timer
	progress_timer pt;
	
	sleep(5); // 5s
	
	return 0;
}