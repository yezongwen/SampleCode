// WaveAudioRecord.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"

#include <conio.h>

#include "sync_simple.h"
#include "waveIN_simple.h"


// Lists WaveIN devices present in the system.
void PrintWaveINDevices() 
{
	const vector<CWaveINSimple*>& wInDevices = CWaveINSimple::GetDevices();
	UINT i;

	for (i = 0; i < wInDevices.size(); i++) 
	{
		printf("%s\n", wInDevices[i]->GetName());
	}
}

// Prints WaveIN device's lines.
void printLines(CWaveINSimple& WaveInDevice) 
{
	CHAR szName[MIXER_LONG_NAME_CHARS];
	UINT j;

	try 
	{
		CMixer& mixer = WaveInDevice.OpenMixer();
		const vector<CMixerLine*>& mLines = mixer.GetLines();

		for (j = 0; j < mLines.size(); j++) 
		{
			::CharToOem(mLines[j]->GetName(), szName);
			printf("%s\n", szName);
		}

		mixer.Close();
	}
	catch (const char *err) 
	{
		printf("%s\n",err);
	}
}

void PrintAllDevicesandLines()
{
	const vector<CWaveINSimple*>& wInDevices = CWaveINSimple::GetDevices();
	UINT i;

	for (i = 0; i < wInDevices.size(); i++) 
	{
		printf("%02d : ", i);
		printf("%s\n", wInDevices[i]->GetName());

		printLines(*wInDevices[i]);
	}
	printf("\n\n");
}

// An example of the IReceiver implementation.
class PcmWriter: public IReceiver {
private:
	FILE * fOut;

public:
	PcmWriter() 
	{
		fOut = fopen("record.pcm", "wb");
		if (fOut == NULL) throw "Can't create MP3 file.";
	};

	~PcmWriter() 
	{
		if (NULL != fOut)fclose(fOut);
	};

	virtual void ReceiveBuffer(LPSTR lpData, DWORD dwBytesRecorded) 
	{

		fwrite(lpData, dwBytesRecorded, 1, fOut);
	};
};

int _tmain(int argc, _TCHAR* argv[])
{
	
	PrintAllDevicesandLines();

	PcmWriter * pcmWr = new PcmWriter;

	// 使用默认的输入设备和默认的line
	CWaveINSimple& device = CWaveINSimple::GetDevice(0);
	device.Start(pcmWr);
	printf("hit <ENTER> to stop ...\n");
	while( !_kbhit() ) ::Sleep(100);

	device.Stop();

	delete pcmWr;
	pcmWr = NULL;

	return 0;
}