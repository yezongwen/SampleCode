
// 2_sdl_yuv_playerDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "2_sdl_yuv_player.h"
#include "2_sdl_yuv_playerDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMy2_sdl_yuv_playerDlg 对话框


enum{
	DFT_WIDTH = 720, 
	DFT_HEIGHT = 576,
	DFT_FPS = 25,

	SHOW_TIMER_ID = WM_USER + 1,
};

CMy2_sdl_yuv_playerDlg::CMy2_sdl_yuv_playerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMy2_sdl_yuv_playerDlg::IDD, pParent)
	, m_width(DFT_WIDTH), m_height(DFT_HEIGHT), m_fps(DFT_FPS)
	, m_in_file(nullptr)
	, m_frame_data(nullptr), m_frame_length(0), m_plane_size(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMy2_sdl_yuv_playerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_WIDTH, m_width);
	DDV_MinMaxUInt(pDX, m_width, 0, 5000);
	DDX_Text(pDX, IDC_EDIT_HEIGHT, m_height);
	DDV_MinMaxUInt(pDX, m_height, 0, 5000);
	DDX_Text(pDX, IDC_EDIT_FPS, m_fps);
}

BEGIN_MESSAGE_MAP(CMy2_sdl_yuv_playerDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(ID_BUTTON_PLAY, &CMy2_sdl_yuv_playerDlg::OnBnClickedButtonPlay)
	ON_WM_TIMER()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CMy2_sdl_yuv_playerDlg 消息处理程序

BOOL CMy2_sdl_yuv_playerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CMy2_sdl_yuv_playerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMy2_sdl_yuv_playerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMy2_sdl_yuv_playerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

bool CMy2_sdl_yuv_playerDlg::InitRender(CString file_path)
{
	UpdateData(TRUE);
	m_plane_size = (m_width * m_height) >> 2;
	m_frame_length = m_plane_size * 6;
	m_frame_data = new unsigned char[m_frame_length];
	if (nullptr == m_frame_data)
	{
		return false;
	}
	m_plane_size <<= 2;

	m_in_file = nullptr;
	if (0 != fopen_s(&m_in_file, (LPCTSTR)file_path, "rb"))
	{
		CString strMsg;
		strMsg.Format("open failed! %s", file_path);
		AfxMessageBox(strMsg);
		return false;
	}

	CRect rect;
	CStatic * pStatic = (CStatic *)GetDlgItem(IDC_STATIC_VIDEO);
	pStatic->GetClientRect(&rect);
	// 因为SDL_DestoryWindow会调用ShowWindow使窗口隐藏
	// 为了实现重复使用播放窗口的目的，这里直接将其显示出来
	pStatic->ShowWindow(SW_SHOW);
	m_yuv_render.Init(pStatic->GetSafeHwnd(), rect);

	ASSERT(0 != m_fps);
	int interval = 1000 / m_fps;
	SetTimer(SHOW_TIMER_ID, interval, NULL);

	return true;
}

void CMy2_sdl_yuv_playerDlg::DeinitRender()
{
	if (nullptr != m_in_file)
	{
		KillTimer(SHOW_TIMER_ID);
		fclose(m_in_file);
		m_in_file = nullptr;
	}

	m_yuv_render.Deinit();

	if (nullptr != m_frame_data)
	{
		delete [] m_frame_data;
		m_frame_data = nullptr;
	}

	m_plane_size = 0;
}

void CMy2_sdl_yuv_playerDlg::OnBnClickedButtonPlay()
{
	CString file_name = _T("");
	CFileDialog fd(TRUE,  NULL, 
		file_name, 
		OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_NOCHANGEDIR, 
		NULL, NULL);
	if (fd.DoModal() == IDOK) 
	{
		DeinitRender();
		InitRender(fd.GetPathName());
	}
}


void CMy2_sdl_yuv_playerDlg::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == SHOW_TIMER_ID && nullptr != m_in_file)
	{
		size_t read_size = fread(m_frame_data, 1, m_frame_length, m_in_file);
		if(read_size == m_frame_length)
		{
			unsigned char *src[3] = {NULL};		//Y、U、V数据首地址
			src[0] = m_frame_data;
			src[1] = src[0] +  m_plane_size;
			src[2] = src[1] + (m_plane_size>>2);
			int stride[3] = {m_width, m_width/2, m_width/2};
			m_yuv_render.Update(m_width, m_height, src, stride);
			m_yuv_render.Render();
		}
		else
		{
			// 循环播放
			fseek(m_in_file, 0, SEEK_SET);
		}
	}

	CDialogEx::OnTimer(nIDEvent);
}


void CMy2_sdl_yuv_playerDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	DeinitRender();

	if (SDL_WasInit(0))SDL_Quit();
}
