
#include "stdafx.h"
#include "SDLVideoRender.h"

SDLVideoRender::SDLVideoRender()
	: m_sdl_window(nullptr)
{

}

SDLVideoRender::~SDLVideoRender()
{
	Deinit();
}

bool SDLVideoRender::Init(HWND show_wnd, RECT show_rect)
{
	// 初始化窗口句柄为空或者显示区域为空
	if (nullptr == show_wnd || IsRectEmpty(&show_rect))
	{
		return false;
	}

	if (nullptr != m_sdl_window)
	{
		return true;
	}

	if (0 == SDL_WasInit(SDL_INIT_VIDEO))
	{
		SDL_InitSubSystem(SDL_INIT_VIDEO);
	}

	m_sdl_window = SDL_CreateWindowFrom(show_wnd);
	if (nullptr == m_sdl_window)
	{
		return false;
	}

	m_show_rect.x = show_rect.left;
	m_show_rect.y = show_rect.top;
	m_show_rect.w = show_rect.right - show_rect.left;
	m_show_rect.h = show_rect.bottom - show_rect.top;

	return true;
}

void SDLVideoRender::Deinit()
{
	if (nullptr != m_sdl_window)
	{
		SDL_DestroyWindow(m_sdl_window);
		m_sdl_window = nullptr;
	}
}