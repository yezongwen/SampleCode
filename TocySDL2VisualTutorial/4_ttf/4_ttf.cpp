// 4_ttf.cpp : 定义控制台应用程序的入口点。
//

/*
This code is from SDL2_ttf 2.0.14 showfont.c
just convert to C++ version
*/

// quiet windows compiler warnings
#define _CRT_SECURE_NO_WARNINGS

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "SDL.h"
#include "SDL_ttf.h"

#define DEFAULT_PTSIZE  18
#define DEFAULT_TEXT    "The quick brown fox jumped over the lazy dog"
#define NUM_COLORS      256
#define WIDTH   640
#define HEIGHT  480

static char *Usage =
"Usage: %s [-solid] [-utf8|-unicode] [-b] [-i] [-u] [-s] [-outline size] [-hintlight|-hintmono|-hintnone] [-nokerning] [-fgcol r,g,b] [-bgcol r,g,b] <font>.ttf [ptsize] [text]\n";

struct Scene
{
	SDL_Texture *caption;
	SDL_Rect captionRect;
	SDL_Texture *message;
	SDL_Rect messageRect;

	void draw(SDL_Renderer *renderer);
};

void Scene::draw(SDL_Renderer *renderer)
{
	/* Clear the background to background color */
	SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(renderer);

	SDL_RenderCopy(renderer, this->caption, NULL, &this->captionRect);
	SDL_RenderCopy(renderer, this->message, NULL, &this->messageRect);
	SDL_RenderPresent(renderer);
}

static void cleanup(int exitcode)
{
	TTF_Quit();
	SDL_Quit();
	exit(exitcode);
}

#ifdef main
#undef main
#endif
int main(int argc, char *argv[])
{
	char *argv0 = argv[0];
	int i, done;
	int rendersolid = 0;
	int renderstyle = TTF_STYLE_NORMAL;
	int outline = 0;
	int hinting = TTF_HINTING_NORMAL;
	int kerning = 1;
	int dump = 0;
	enum {
		RENDER_LATIN1,
		RENDER_UTF8,
		RENDER_UNICODE
	} rendertype = RENDER_LATIN1;
	char *message, string[128];

	/* Default is black and white */
	SDL_Color white = { 0xFF, 0xFF, 0xFF, 0 };
	SDL_Color black = { 0x00, 0x00, 0x00, 0 };
	SDL_Color *forecol = &black;
	SDL_Color *backcol = &white;

	// 解析命令行参数
	for (i = 1; argv[i] && argv[i][0] == '-'; ++i) {
		if (strcmp(argv[i], "-solid") == 0) {
			rendersolid = 1;
		}
		else if (strcmp(argv[i], "-utf8") == 0) {
			rendertype = RENDER_UTF8;
		}
		else if (strcmp(argv[i], "-unicode") == 0) {
			rendertype = RENDER_UNICODE;
		}
		else if (strcmp(argv[i], "-b") == 0) {
			renderstyle |= TTF_STYLE_BOLD;
		}
		else if (strcmp(argv[i], "-i") == 0) {
			renderstyle |= TTF_STYLE_ITALIC;
		}
		else if (strcmp(argv[i], "-u") == 0) {
			renderstyle |= TTF_STYLE_UNDERLINE;
		}
		else if (strcmp(argv[i], "-s") == 0) {
			renderstyle |= TTF_STYLE_STRIKETHROUGH;
		}
		else if (strcmp(argv[i], "-outline") == 0) {
			if (sscanf(argv[++i], "%d", &outline) != 1) {
				fprintf(stderr, Usage, argv0);
				return(1);
			}
		}
		else if (strcmp(argv[i], "-hintlight") == 0) {
			hinting = TTF_HINTING_LIGHT;
		}
		else if (strcmp(argv[i], "-hintmono") == 0) {
			hinting = TTF_HINTING_MONO;
		}
		else if (strcmp(argv[i], "-hintnone") == 0) {
			hinting = TTF_HINTING_NONE;
		}
		else if (strcmp(argv[i], "-nokerning") == 0) {
			kerning = 0;
		}
		else if (strcmp(argv[i], "-dump") == 0) {
			dump = 1;
		}
		else if (strcmp(argv[i], "-fgcol") == 0) {
			int r, g, b;
			if (sscanf(argv[++i], "%d,%d,%d", &r, &g, &b) != 3) {
				fprintf(stderr, Usage, argv0);
				return(1);
			}
			forecol->r = (Uint8)r;
			forecol->g = (Uint8)g;
			forecol->b = (Uint8)b;
		}
		else if (strcmp(argv[i], "-bgcol") == 0) {
			int r, g, b;
			if (sscanf(argv[++i], "%d,%d,%d", &r, &g, &b) != 3) {
				fprintf(stderr, Usage, argv0);
				return(1);
			}
			backcol->r = (Uint8)r;
			backcol->g = (Uint8)g;
			backcol->b = (Uint8)b;
		}
		else {
			fprintf(stderr, Usage, argv0);
			return(1);
		}
	}
	argv += i;
	argc -= i;

	/* Check usage */
	if (!argv[0]) {
		fprintf(stderr, Usage, argv0);
		return(1);
	}

	/* 初始化TTF库 */
	if (TTF_Init() < 0) {
		fprintf(stderr, "Couldn't initialize TTF: %s\n", SDL_GetError());
		SDL_Quit();
		return(2);
	}

	/* 打开字体库并按照字体大小初始化 */
	int ptsize = 0;
	if (argc > 1) {
		ptsize = atoi(argv[1]);
	}
	if (ptsize == 0) {
		i = 2;
		ptsize = DEFAULT_PTSIZE;
	}
	else {
		i = 3;
	}

	TTF_Font * font = TTF_OpenFont(argv[0], ptsize);
	if (font == NULL) {
		fprintf(stderr, "Couldn't load %d pt font from %s: %s\n",
			ptsize, argv[0], SDL_GetError());
		cleanup(2);
	}
	// 字体显示参数设置
	TTF_SetFontStyle(font, renderstyle);
	TTF_SetFontOutline(font, outline);
	TTF_SetFontKerning(font, kerning);
	TTF_SetFontHinting(font, hinting);

	// 如果设置导出参数，直接将字体保存为位图
	if (dump) {
		for (i = 48; i < 123; i++) {
			SDL_Surface* glyph = NULL;

			glyph = TTF_RenderGlyph_Shaded(font, i, *forecol, *backcol);

			if (glyph) {
				char outname[64];
				sprintf(outname, "glyph-%d.bmp", i);
				SDL_SaveBMP(glyph, outname);
			}

		}
		cleanup(0);
	}

	SDL_Window *window;
	SDL_Renderer *renderer;
	/* 创建SDL窗口 */
	if (SDL_CreateWindowAndRenderer(WIDTH, HEIGHT, 0, &window, &renderer) < 0) {
		fprintf(stderr, "SDL_CreateWindowAndRenderer() failed: %s\n", SDL_GetError());
		cleanup(2);
	}

	SDL_Surface *text;
	Scene scene;
	/* 显示标题栏的字体参数 */
	sprintf(string, "Font file: %s", argv[0]);  /* possible overflow */
	if (rendersolid) {
		text = TTF_RenderText_Solid(font, string, *forecol);
	}
	else {
		text = TTF_RenderText_Shaded(font, string, *forecol, *backcol);
	}
	if (text != NULL) {
		scene.captionRect.x = 4;
		scene.captionRect.y = 4;
		scene.captionRect.w = text->w;
		scene.captionRect.h = text->h;
		scene.caption = SDL_CreateTextureFromSurface(renderer, text);
		SDL_FreeSurface(text);
	}

	/* 显示和初始化跟随消息的参数 */
	if (argc > 2) {
		message = argv[2];
	}
	else {
		message = DEFAULT_TEXT;
	}
	switch (rendertype) {
	case RENDER_LATIN1:
		if (rendersolid) {
			text = TTF_RenderText_Solid(font, message, *forecol);
		}
		else {
			text = TTF_RenderText_Shaded(font, message, *forecol, *backcol);
		}
		break;

	case RENDER_UTF8:
		if (rendersolid) {
			text = TTF_RenderUTF8_Solid(font, message, *forecol);
		}
		else {
			text = TTF_RenderUTF8_Shaded(font, message, *forecol, *backcol);
		}
		break;

	case RENDER_UNICODE:
	{
		Uint16 *unicode_text = SDL_iconv_utf8_ucs2(message);
		if (rendersolid) {
			text = TTF_RenderUNICODE_Solid(font,
				unicode_text, *forecol);
		}
		else {
			text = TTF_RenderUNICODE_Shaded(font,
				unicode_text, *forecol, *backcol);
		}
		SDL_free(unicode_text);
	}
	break;
	default:
		text = NULL; /* This shouldn't happen */
		break;
	}
	if (text == NULL) {
		fprintf(stderr, "Couldn't render text: %s\n", SDL_GetError());
		TTF_CloseFont(font);
		cleanup(2);
	}
	scene.messageRect.x = (WIDTH - text->w) / 2;
	scene.messageRect.y = (HEIGHT - text->h) / 2;
	scene.messageRect.w = text->w;
	scene.messageRect.h = text->h;
	scene.message = SDL_CreateTextureFromSurface(renderer, text);
	printf("Font is generally %d big, and string is %d big\n",
		TTF_FontHeight(font), text->h);

	// 渲染
	scene.draw(renderer);

	/* 处理键盘和鼠标消息 */
	done = 0;
	SDL_Event event;
	while (!done) {
		if (SDL_WaitEvent(&event) < 0) {
			fprintf(stderr, "SDL_PullEvent() error: %s\n",
				SDL_GetError());
			done = 1;
			continue;
		}
		switch (event.type) {
		case SDL_MOUSEBUTTONDOWN:
			scene.messageRect.x = event.button.x - text->w / 2;
			scene.messageRect.y = event.button.y - text->h / 2;
			scene.messageRect.w = text->w;
			scene.messageRect.h = text->h;
			scene.draw(renderer);
			break;

		case SDL_KEYDOWN:
		case SDL_QUIT:
			done = 1;
			break;
		default:
			break;
		}
	}

	// 资源销毁及释放
	SDL_FreeSurface(text);
	TTF_CloseFont(font);
	SDL_DestroyTexture(scene.caption);
	SDL_DestroyTexture(scene.message);
	cleanup(0);

	/* Not reached, but fixes compiler warnings */
	return 0;
}

