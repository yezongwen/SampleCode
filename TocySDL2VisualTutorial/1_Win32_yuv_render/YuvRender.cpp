#include "StdAfx.h"
#include "YuvRender.h"
#include <cstdio>

YuvRender::YuvRender(void)
	: SDLVideoRender()
	, m_yuv_width(720), m_yuv_height(576)
	, m_yuv_frame_size(0)
	, m_yuv_data(nullptr)
	, m_show_texture(nullptr)
{

}

YuvRender::~YuvRender(void)
{
	Deinit();
}

bool YuvRender::Init(HWND show_wnd, RECT show_rect)
{
	if (!SDLVideoRender::Init(show_wnd, show_rect))
		return false;

	// create texture
	m_show_texture = SDL_CreateTexture(m_sdl_renderer, SDL_PIXELFORMAT_IYUV, 
		SDL_TEXTUREACCESS_STREAMING, m_yuv_width, m_yuv_height);
	if (nullptr == m_show_texture)
	{
		return false;
	}

	// alloc buffer and read yuv file
	m_yuv_frame_size = m_yuv_width * m_yuv_height * 3 >> 1;
	m_yuv_data = new unsigned char[m_yuv_frame_size];
	FILE * fin = NULL;
	if (0 != fopen_s(&fin, "test_720x576.yuv", "rb"))
	{
		return false;
	}

	fread(m_yuv_data, 1, m_yuv_frame_size, fin);

	fclose(fin);
	
	return true;
}
void YuvRender::Deinit()
{
	if (nullptr != m_yuv_data)
	{
		delete [] m_yuv_data;
		m_yuv_data = nullptr;
	}

	if (nullptr != m_show_texture)
	{
		SDL_DestroyTexture(m_show_texture);
		m_show_texture = NULL;
	}

	SDLVideoRender::Deinit();
}

void YuvRender::FillTexture1()
{
	// This is a fairly slow function, intended for use with static textures that do not change often
	SDL_UpdateTexture(m_show_texture, NULL, m_yuv_data, m_yuv_width);
}

void YuvRender::FillTexture2()
{
	void * pixel = NULL;
	int pitch = 0;
	if(0 == SDL_LockTexture(m_show_texture, NULL, &pixel, &pitch))
	{
		// 如果不考虑数据对齐，直接拷贝YUV数据是没有问题的
		if (pitch == m_yuv_width)
		{
			memcpy(pixel, m_yuv_data, m_yuv_frame_size);
		}
		else // 可能发生pitch > width的情况
		{
			// 如果有数据对齐的情况，单独拷贝每一行数据
			// for Y
			int h = m_yuv_height;
			int w = m_yuv_width;
			unsigned char * dst = reinterpret_cast<unsigned char *>(pixel);
			unsigned char * src = m_yuv_data;
			for (int i = 0; i < h; ++i)
			{
				memcpy(dst, src, w);
				dst += pitch;
				src += w;
			}

			h >>= 1;
			w >>= 1;
			pitch >>= 1;
			// for U
			for (int i = 0; i < h; ++i)
			{
				memcpy(dst, src, w);
				dst += pitch;
				src += w;
			}

			// for V
			for (int i = 0; i < h; ++i)
			{
				memcpy(dst, src, w);
				dst += pitch;
				src += w;
			}
		}

		SDL_UnlockTexture(m_show_texture);
	}
}

bool YuvRender::Render()
{
	if (NULL != m_show_texture)
	{
		//FillTexture1();
		FillTexture2();

		SDL_RenderCopy(m_sdl_renderer, m_show_texture, NULL, &m_show_rect);    
		SDL_RenderPresent(m_sdl_renderer); 
	}

	return true;
}
