/* 
 * Invoke c++ function and class in C Language
 * Copyright (c) Tocy <zyvj@qq.com>
*/

#ifndef C_INVOKE_CPP_INCLUDE_HEADER_FILE_
#define C_INVOKE_CPP_INCLUDE_HEADER_FILE_

void DenyFunc();

void OverloadFunc(int param, bool is_c=false);
void OverloadFunc(double param, bool is_c=false);

class AType
{
public:
	AType();
	~AType();
	
	void MemFunc(int value);
};

extern "C"
{
	int FunCppDecorate(int param);
	
	/* 以下实现参考普通函数调用，模拟重载、类成员函数调用*/
	void OverloadDecorate_i(int param);
	void OverloadDecorate_d(double param);
	
	
	struct TagAType * CreateInstance();
	void DestoryInstance(struct TagAType ** atype);
	void ClassMemFunc(struct TagAType * pthis, int param);
	
}

#endif //#ifndef C_INVOKE_CPP_INCLUDE_HEADER_FILE_