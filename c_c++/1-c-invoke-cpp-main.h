/* 
 * Invoke c++ function and class in C Language
 * Copyright (c) Tocy <zyvj@qq.com>
 *
 * Compile in GCC 4.8.1
 * refer to c-invoke-cpp.h
 
*/

#ifndef C_INVOKE_CPP_MAIN_INCLUDE_HEADER_FILE_
#define C_INVOKE_CPP_MAIN_INCLUDE_HEADER_FILE_

void DenyFunc();
int FunCppDecorate(int param);

void OverloadDecorate_i(int param);
void OverloadDecorate_d(double param);

struct TagAType;
struct TagAType * CreateInstance();
void DestoryInstance(struct TagAType ** atype);
void ClassMemFunc(struct TagAType * pthis, int param);

#endif //#ifndef C_INVOKE_CPP_MAIN_INCLUDE_HEADER_FILE_