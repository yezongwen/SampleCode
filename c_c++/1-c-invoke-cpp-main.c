/* 
 * Invoke c++ function and class in C Language
 * Copyright (c) Tocy <zyvj@qq.com>
 *
 * Compile in GCC 4.8.1
 *
 * compile as follows:
 *     g++ -c 1-c-invoke-cpp.cpp
 *     gcc -c 1-c-invoke-cpp-main.c
 *     gcc 1-c-invoke-cpp.o 1-c-invoke-cpp-main.o -o invoke.exe
 *
 * You can see *.o use "nm a.o"
*/

#include <stdio.h>
#include "1-c-invoke-cpp-main.h"


int main(int argc, char * argv[])
{
	// link give "undefined reference"
	//DenyFunc();
	
	FunCppDecorate(10);
	
	OverloadDecorate_i(1);
	
	OverloadDecorate_d(2.0);
	
	struct TagAType * obj = CreateInstance();
	ClassMemFunc(obj, 12);
	DestoryInstance(&obj);
	
	return 0;
}