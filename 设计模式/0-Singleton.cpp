
// c++中线程安全的单例模式实现代码
// 注意线程同步机制方面是伪代码，需要是实际平台选择
// 比如windows下使用CriticalSection，boost中的同步机制

/* 静态成员的单例模式 */
class LazySingleton
{
public:
	static LazySingleton & GetInstance()
	{
		return m_instance;
	}
	
private:
	static LazySingleton m_instance;
	
	LazySingleton();
	~LazySingleton();
};

LazySingleton LazySingleton::m_instance;

/* 加锁的动态分配版本 */
class AccessLockSingleton
{
public:
	static AccessLockSingleton * GetInstance()
	{
		Lock(); // compile error for Lock/Unlock
		if (nullptr == m_instance)
			m_instance = new AccessLockSingleton;
		
		Unlock();
		
		return m_instance;
	}
	
private:
	static AccessLockSingleton * m_instance;
	
	AccessLockSingleton();
	~AccessLockSingleton();
};
AccessLockSingleton * AccessLockSingleton::m_instance = nullptr;

/* 优化的多线程安全版本 */
class ThreadSafeSingleton
{
public:
	ThreadSafeSingleton * GetInstance()
	{
		if (nullptr == m_instance)
		{
			Lock();// compile error for Lock/Unlock
			if (nullptr == m_instance)
				m_instance = new ThreadSafeSingleton;
			Unlock();
		}
		
		return m_instance;
	}
	
private:
	static ThreadSafeSingleton * m_instance;
	
	ThreadSafeSingleton();
	~ThreadSafeSingleton();
}；
