// 责任链模式示例代码
// 核心在于“将请求交给责任链，尤其决定如何处理”

#include<iostream>
using namespace std;

// 请求和处理界别类
// 这里用0表示guest，1表示user，2表示admin
enum Level
{
	LEVEL_GUEST = 0,
	LEVEL_USER = 1,
	LEVEL_ADMIN = 2
};

// 实际请求
class Request
{
public:
	Request(Level req):m_request(req){}
	Level GetRequestLevel() const
	{return m_request;}
	
private:
	Level m_request;
};

// 处理结果
class Response
{
public:
	Response();
};

// 抽象处理者
class Handler
{
public:
	Handler(): m_next_handler(NULL){}
	virtual ~Handler(){}
	
	Response* HandleRequeset(const Request& r)
	{
		if (GetHandlerLevel() == r.GetRequestLevel())
		{
			return Handle(r);
		}
		else if (NULL != m_next_handler)
		{
			return m_next_handler->HandleRequeset(r);
		}
		else
		{
			// 没有适当的处理者，需要自动处理下或提醒
		}
	}
	void SetNext(Handler* h)
	{m_next_handler=h;}
	
protected:
	virtual Level GetHandlerLevel()=0;
	virtual Response * Handle(const Request& r)=0;
	
private:
	Handler * m_next_handler;
};

/* 具体处理者 */
// 责任链为guest-user-admin
class Guest: public Handler
{
public:
	Guest(): Handler(){}

protected:
	Level GetHandlerLevel()
	{return LEVEL_GUEST;}
	Response * Handle(const Request& r)
	{return NULL;}
};

class User: public Handler
{
public:
	User(): Handler(){}

protected:
	Level GetHandlerLevel()
	{return LEVEL_USER;}
	Response * Handle(const Request& r)
	{return NULL;}
};

class Admin: public Handler
{
public:
	Admin(): Handler(){}

protected:
	Level GetHandlerLevel()
	{return LEVEL_ADMIN;}
	Response * Handle(const Request& r)
	{return NULL;}
};

int main(int argc, char* argv[])
{
	// 建立责任链
	Guest guest;
	User user;
	Admin admin;
	guest.SetNext(&user);
	user.SetNext(&admin);
	
	// 请求User处理
	Handler& handler = guset;
	Request user_req(LEVEL_USER);
	guest.HandleRequeset(user_req);
	
	return 0;
}
