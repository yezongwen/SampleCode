// Windows错误码解析程序，ErrCodeParseDemo
// 简单介绍如何将错误码转化为对应的字符串信息
// 建议使用vs2005以上版本编译 unicode编码
#include <windows.h> 
#include <iostream>
#include <WinError.h>
using std::wcout;
using std::endl;

void OutputFormatMessage(DWORD errCode)
{
	LPTSTR lpMsgBuf = NULL;

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		errCode,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR) &lpMsgBuf,
		0, NULL );

	wcout << "err:" << std::hex << errCode << " Msg tips:" << endl
		<< lpMsgBuf << endl;

	 LocalFree(lpMsgBuf);
}

int _tmain(int argc, _TCHAR* argv[])
{
	// 输出中文
	std::wcout.imbue(std::locale("chs"));

	// 设置控制台标题栏
	SetConsoleTitle(TEXT("ErrCodeParseDemo"));

	OutputFormatMessage(ERROR_INVALID_FUNCTION);

	OutputFormatMessage(ERROR_HANDLE_EOF);

	return 0; 
}