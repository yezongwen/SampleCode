/* compile in gcc 4.8.1
 * g++ -std=c++11 cpp_primer_test3.cpp
*/
#include <iostream>

// for testing , write using refrence here
using std::cout;
using std::endl;

class CtorDftType
{
public:
	CtorDftType()=default;
	CtorDftType(const CtorDftType&)=default;
	CtorDftType & operator = (const CtorDftType &)=default;
	~CtorDftType()=default;
};

class DelModType
{
public:
	DelModType()=default;
	DelModType(const DelModType&)=delete;// no copy ctor
	DelModType& operator = (const DelModType&)=delete; // no assignment
	~DelModType() = default;
};

class ExplicitConversion
{
public:
	explicit operator bool() const{return true;}
};

// test code for override qualifier
class Base
{
public:
	Base()=default;
	virtual ~Base()=default;
	virtual int GetType() const{return 0;}
};
class Derived: public Base
{
public:
	Derived()=default;
	~Derived()=default;
	int GetType() const override{return 1;}
};

// test code for inherited ctors
class InBase
{
public:
	InBase(int x, int y): m_x(x), m_y(y){}
	virtual ~InBase(){}
protected:
	int m_x, m_y;
};
class InDerived: public InBase
{
public:
	using InBase::InBase;
};

int main(int argc, char ** argv)
{		
	CtorDftType cdt;
	DelModType dmt;
	InDerived idr{1,2};
	return 0;
}

