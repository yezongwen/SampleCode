/* compile in gcc 4.8.1
 * g++ -std=c++11 cpp_primer_test1.cpp
*/
#include <iostream>
#include <string>
#include <initializer_list>

// for testing , write using refrence here
using std::cout;
using std::endl;
using std::string;
using std::initializer_list;

// 测试变长参数
void TestVariadicParams(initializer_list<int> iz)
{	
	cout << "initializer_list test" << endl;
	for (const auto & r: iz)
		cout << r << " ";
	cout << endl;
}

// 函数返回类型后置 trailing return type
// 返回值为指向int[10]的指针
auto Func(int) -> int(*)[10];

// 返回指向even[4]的指针
int even[] = {2,4,6,8};
decltype(even) * GetPtr(int); 

constexpr int DefaultSize(){return 5;}
int ArraySize(int i)
{return i*DefaultSize();}
constexpr int ArrayConstSize(int i)
{return i*DefaultSize();}
void TestConstExpr()
{
	int a1[ArraySize(5)];
	int a2[ArrayConstSize(5)];
	int i = 5;
	int a3[ArraySize(i)];
	int a4[ArrayConstSize(i)];
}

// delegating constructor
class DAType
{
public:
	DAType(int x, int y):m_x(x), m_y(y){}
	DAType(int y): DAType(0,y){}
	
private:
	int m_x;
	int m_y;
};

int main(int argc, char ** argv)
{
	TestVariadicParams({1,4,7});
	
	return 0;
}

