/* compile in gcc 4.8.1
 * g++ -std=c++11 thread_util_sample_param
*/

// thread util sample 2
void TaskFunc(int i)
{}

class TaskObj
{
public:
	TaskObj(int i):m_value(i){}
	void operator()()
	{m_value=123;}
private:
	int m_value{0};
};

int main(int argc, char ** argv)
{	
	int cur_value{100};
	thread t_func{TaskFunc, cur_value};
	thread t_obj{TaskObj{cur_value}};
	
	t_func.join();
	t_obj.join();
	return 0;
}

