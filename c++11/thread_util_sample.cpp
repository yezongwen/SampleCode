/* compile in gcc 4.8.1
 * g++ -std=c++11 thread_util_sample.cpp
*/

#include <iostream>
#include <thread>
using namespace std;

void TaskFunc()
{}

class TaskObj
{
public:
	TaskObj(){}
	void operator()()
	{}
};

int main(int argc, char ** argv)
{	
	thread t_func{TaskFunc};
	thread t_obj{TaskObj()};
	
	t_func.join();
	t_obj.join();
	return 0;
}

