/* compile in gcc 4.8.1
 * g++ -std=c++11 cpp_primer_test4.cpp
*/
#include <iostream>

// for testing , write using refrence here
using std::cout;
using std::endl;

// enum
enum {ZERO, ONE, TWO};
enum class color {RED, BLUE, GREEN};

enum CtrlType: unsigned long long
{CT_0, CT_1};

// enum forward declaration
enum InitValues: long long;
enum class NewColor;

// nested class
class OuterClass
{
	class InnerClass;
};

class OuterClass::InnerClass
{
	
};

#include<string>
// union with class member
union UnionTest
{
	int a;
	std::string str;
};

int main(int argc, char ** argv)
{	
	int ne = ONE;
	color se_color = color::GREEN;
	return 0;
}

