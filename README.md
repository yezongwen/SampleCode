# SampleCode
---
本工程主要包含本人测试及练习使用的代码。其中包含的内容如下：
### c++11
主要内容是针对c++11标准中新添加语法和功能的总结。

### Console
主要是学习windows控制台时完善的控制台有关操作的测试代码。

### TDSDL2Tutorial
参考网上的SDL2入门资料整理的在vs10下编译的版本。

### TocySDL2VisualTutorial
本人学习SDL2时整理的资料，主要是关于音视频渲染的机制。

### 设计模式
在c++中实现23种设计模式的测试代码。

### WaveInAudioRecord
基于WaveIn的音频录制demo，输出为PCM数据。

### ScreenCaptureApiTest
验证Windows下GDI/DirectX抓屏性能的测试demo。
主要是抓取1500帧图像的时间，并输出。

### boost
主要是包含boost学习及测试有关的代码。

### makefile-template
包含GNU make编译所需的模板，可执行文件、静态库、动态库

### JsonCppTutorial
简单的JsonCpp用法，包含JSON解析和输出。






