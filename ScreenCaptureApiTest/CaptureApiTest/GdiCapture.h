
#ifndef GDI_CAPTURE_FILE_HEADER_INCLUDE_20141104
#define GDI_CAPTURE_FILE_HEADER_INCLUDE_20141104

void CaptureScreenshotbyGdi();

namespace Screenshot
{	
	class GdiCapture
	{
	public:
		GdiCapture();
		~GdiCapture();
		
		void Init();
		void Deinit();

		bool IsInit() const;

		void CaptureFrame();

	private:

		int width, height;
		HWND hDesktopWnd;
		HDC hDesktopDC;
		HDC hDesktopCompatibleDC;
		HBITMAP hDesktopCompatibleBitmap;
		HDC hBmpFileDC;
		HBITMAP	hBmpFileBitmap;

	};
}

#endif // #ifndef GDI_CAPTURE_FILE_HEADER_INCLUDE_20141104