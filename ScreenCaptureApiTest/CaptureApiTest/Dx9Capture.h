
#ifndef DIRECTX9_CAPTURE_HEADER_FILE_INCLUDE_20141104
#define DIRECTX9_CAPTURE_HEADER_FILE_INCLUDE_20141104

namespace Screenshot
{
	class Dx9Capture
	{
	public:
		Dx9Capture();
		~Dx9Capture();

		void Init(HWND hWnd);
		void Uninit();

		bool IsInit() const;
		void FrontBufferFrame();
		void RenderTargetFrame();
		void BackBufferFrame();

	private:
		void Render();
		void Reset();

	private:

		IDirect3D9* g_pD3D;
		IDirect3DDevice9* g_pd3dDevice;
		IDirect3DSurface9* g_pSurface;

		HWND ghWnd;

	};
}

#endif