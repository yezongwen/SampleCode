/* 
 * using shared lib test
 * Copyright (c) Tocy <zyvj@qq.com>
 */

# include <iostream>
#include "pro_lib.h"

int main(int argc, char ** argv)
{
    using std::cout;
    using std::endl;

    cout << argv[0] << ", welcome to shared lib test!" << endl;
    
    regard2invoker(0);
    regard2invoker(1);

    return 0;
}