Note there are some differences in Windows and Linux(my Linux is Ubuntu 15 gcc 4.9.2)

## Linux --- Ubuntu gcc
If you use this sample in ubuntu, in order to run the shared_check, you need to modify `LD_LIBRARY_PATH` or refer to the [url](http://blog.csdn.net/sahusoft/article/details/7388617)

## Window mingw 
If you build this sample in Windows with gcc/mingw, before compile this sample, you need add `.lib` to  `LD_LIBRARY_PATH`. After this, you need to copy the *.so and the *.exe to same folder to run the exe.


