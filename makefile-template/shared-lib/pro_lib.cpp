/* 
 * static & shared lib implementation file
 * Copyright (c) Tocy <zyvj@qq.com>
 */

#include "pro_lib.h"
#include <iostream>

void regard2invoker(int id)
{
    if (0 == id)
        std::cout <<"This is a message from " << __FUNCTION__ << ", welcome admin id:" << id 
                << "!\n";
    else
        std::cout << "Regards to our invoker from " << __FUNCTION__ << ", your id is " << id
                << "!\n";
}